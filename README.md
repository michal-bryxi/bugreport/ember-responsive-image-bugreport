# ember-responsive-image-bugreport

## How to reproduce:

1. Clone this repo
2. Install & Start
```sh
pnpm install; pnpm start
```
3. Navigate to: http://localhost:4200/
4. See that the page explodes with:
```
runtime.js:4985  

Error occurred:

- While rendering:
  -top-level
    application
      responsive-image


execute @ runtime.js:4985
index.js:128  Uncaught Error: Assertion Failed: There is no data for image assets/images/test.png
    at _assert (index.js:128:1)
    at ResponsiveImageLocalService.getMeta (responsive-image-local.js:114:101)
    at ResponsiveImageLocalService.getAvailableTypes (responsive-image-local.js:126:17)
    at provider (responsive-image-local-provider.js:78:25)
    at get providerResult (responsive-image.js:114:152)
    at getValue (validator.js:596:1)
    at descriptor.get (index.js:21:88)
    at get sources [as sources] (responsive-image.js:121:19)
    at get sourcesSorted [as sourcesSorted] (responsive-image.js:156:17)
    at getPossibleMandatoryProxyValue (index.js:1660:1)
```
5. See that respective images have been generated:
```
ls --tree dist/
dist
├── assets
│  ├── images
│  │  ├── test640w.png
│  │  ├── test640w.webp
│  │  ├── test750w.png
│  │  ├── test750w.webp
│  │  ├── test1080w.png
│  │  ├── test1080w.webp
│  │  ├── test1536w.png
│  │  ├── test1536w.webp
│  │  ├── test2048w.png
│  │  └── test2048w.webp
│  ├── chunk.94c60436dd0946c84470.js
│  ├── chunk.bc5dd00dbec2a625dfbd.js
│  ├── chunk.e0d3ac40e24637409148.js
│  ├── chunk.e9e2187706e2e2cf2a33.js
│  ├── chunk.e305f15c66f76e59f7e5.js
│  ├── ember-responsive-image-bugreport.css
│  ├── test-support.css
│  ├── test-support.css.map
│  ├── test-support.js
│  ├── test-support.map
│  ├── vendor.css
│  ├── vendor.css.map
│  ├── vendor.js
│  └── vendor.map
├── ember-welcome-page
│  └── images
│     └── construction.png
├── tests
│  └── index.html
├── index.html
├── robots.txt
└── testem.js
```
6. Observe steps to reproduce via individual commits in this repo

## Notes

- I also created a [branch without embroider](https://gitlab.com/michal-bryxi/bugreport/ember-responsive-image-bugreport/-/tree/without-embroider?ref_type=heads) to prove that it is the root issue.
- Issue is [reported here](https://github.com/simonihmig/ember-responsive-image/issues/486).
